import ColorsModule from './colors'
import NumbersModule from './numbers'

const Convert = {
  ColorsModule,
  NumbersModule
}

export default Convert
